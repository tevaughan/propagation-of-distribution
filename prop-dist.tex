\documentclass[12pt]{article}

\usepackage{amssymb}  % \mathbb
\usepackage{amsmath}  % \text
\usepackage[margin=10pt,font=small,labelfont=bf,labelsep=endash]{caption}
\usepackage{gnuplottex} % gnuplottex
\usepackage{graphicx}   % \includegraphics
\usepackage{hyperref}   % \url

\title{Propagation of a Probability-Distribution}
\author{Thomas E. Vaughan}

\begin{document}

\maketitle

\begin{abstract}
  A probability-distribution can be transformed by propagating it analytically
  and exactly through a differentiable function, from the function's input to
  its output.  Let $f: \mathbb{R} \rightarrow \mathbb{R}$ be a differentiable
  function, whose value at a measured quantity $x$ is $f(x)$.  Let $p:
  \mathbb{R} \rightarrow \mathbb{R}$ be a normalised distribution of
  probability-density, so that $p(x)$ is the density associated with the
  measured quantity $x$.  Then the normalised distribution of
  probability-density for the quantity $y = f(x)$ is given by a function $q$,
  where
  \[
    q(y) = \sum_k \frac{p(x_k)}{|f'(x_k)|}.
  \]
  The index $k$ is for the solutions $x_k$ to the equation $f(x) = y$.
\end{abstract}


\section{Latest Version of This Document}

\url{https://gitlab.com/tevaughan/propagation-of-distribution}


\section{Introduction}

When I was in graduate-school, there was an occasion on which I wanted to
propagate an error-distribution through a function.  I wanted to solve the
problem analytically, not numerically.  Also, I wanted to solve it without the
usual approximation, in which the initial and resultant distributions are taken
to be Gaussian.  I do not now recall what the specific problem was, back in the
first half of the 1990s, but I do recall how I arrived at the solution to the
general problem.

Even after looking through a few textbooks on probability and statistics and
after looking through several versions of {\it CRC Standard Mathematical
Tables}, I found at the time no treatment of the matter.  A friend has, in
recent years, pointed me to a book that does treat this problem, but I am not
finding the reference as I write this.  In any event, at least as I vaguely
recall the material in that book, I prefer my approach.


\section{The Discrete Problem}

Back in the early 1990s, I did find the treatment of a similar problem for a
discrete distribution.\footnote{I do not have the reference, but I recall that
I found the treatment of the discrete problem in a book by CRC~Press.}  I
reproduce an approximation of it in the present section from memory.

Let $F: \mathbb{N} \rightarrow \mathbb{N}$ be some function, and let $P:
\mathbb{N} \rightarrow \mathbb{R}$ be a normalized, discrete
probability-distribution, so that $P(X)$ is the probability of measuring $X$.
In the limit of a large number of measurements of $X$, the distribution $Q$ of
the derived quantities, each given
by $Y = F(X)$, is
\begin{equation}
  Q(Y) = \sum_{X \in \mathbb{N}} P(X) \; \delta_{Y,F(X)},
  \label{eq:kronecker}
\end{equation}
\noindent where $\delta$ is Kronecker's delta.

Note that, on the one hand, if $F$ be one-to-one, then the delta picks out
exactly one term from the sum.  In that case, for every $Y$,
\begin{equation}
  Q(Y) = P(F^{-1}(Y)),
\end{equation}
\noindent or, equivalently, for every $X$,
\begin{equation}
  Q(F(X)) = P(X).
\end{equation}
\noindent On the other hand, the problem is more interesting whenever there
exist two or more distinct measurements $X_1$, $X_2$, $\ldots$, $X_n$ such that
$F(X_1) = F(X_2) = \cdots = F(X_n)$.  For example, suppose that there are
exactly two, distinct measurements $X_1$ and $X_2$ such that $Y = F(X_1) =
F(X_2)$ for some unique $Y$.  In that case, for that particular $Y$,
\begin{equation}
  Q(Y) = Q(F(X_1)) = Q(F(X_2)) = P(X_1) + P(X_2).
\end{equation}


\section{The Continuous Problem}

The discrete problem above is simple and easy to understand.  The solution is
almost completely obvious.  But seeing it in print gave me an idea.  I had just
recently taken a course in statistical mechanics at the graduate level, and so
I had become extremely familiar with Dirac's delta.  It seemed to me that there
ought to be a way to generalize the result from Kronecker to Dirac.

Let $f: \mathbb{R} \rightarrow \mathbb{R}$ be some function, and let $p:
\mathbb{R} \rightarrow \mathbb{R}$ be a normalized probability-density
function, so that $p(x)$ is the density associated with measuring $x$.  In the
limit of a large number of measurements of $x$, the density $q$ for the derived
quantities, each given by $y = f(x)$, is
\begin{equation}
  q(y) = \int_{-\infty}^{+\infty} p(x) \; \delta(y - f(x)) \; \text{d}x,
  \label{eq:dirac}
\end{equation}
\noindent where $\delta$ is Dirac's delta.  Notice the similarity between
Equation~\ref{eq:kronecker} and Equation~\ref{eq:dirac}.

It is easy to see that $q$ is a normalised distribution.  First, integrate both
sides over $y$.  Then carry out the integral over $y$ on the right side; this
collapses the delta to unity and eliminates the integral over $y$.  All that is
left is the integral of $p$ over $x$, but $p$ is normalised by supposition.  So
$q$ is normalised.

In order to evaluate Equation~\ref{eq:dirac}, let $h_y: \mathbb{R} \rightarrow
\mathbb{R}$ be defined as
\begin{equation}
  h_y(x) = y - f(x).
\end{equation}
\noindent Then we have
\begin{equation}
  q(y) = \int_{-\infty}^{+\infty} p(x) \; \delta(h_y(x)) \; \text{d}x
       = \sum_k \frac{p(x_k)}{|h_y'(x_k)|}.
\end{equation}
\noindent The index $k$ is for each zero $x_k$ of $h_y$.  In our case, $k$ is
the index for each of the solutions $x_k$ to $f(x) = y$, and we have at last
\begin{equation}
  q(y) = \sum_k \frac{p(x_k)}{|f'(x_k)|}.
\end{equation}


\begin{figure}%
  \begin{gnuplot}[terminal=cairolatex]
    unset key
      set xrange [0:1]
      set yrange [0:10]
      set xlabel '$x$'
      set ylabel '$f(x)$'
    plot (1+x)/(1-x)
  \end{gnuplot}
  \caption{Example function, $f(x) = \frac{1+x}{1-x}$, for transforming
    each measurement $x$ drawn from a distribution characterised by a
    probability-density function.%
  }\label{fig:f}
\end{figure}

\begin{figure}
  \begin{gnuplot}[terminal=cairolatex]
    unset key
      set xrange [1:10]
      set yrange [0:0.55]
      set xlabel '$y$'
      set ylabel '$q(y)$'
    plot 0.5*(1 - (x-1)/(x+1))**2
  \end{gnuplot}
  \caption{Distribution of probability density for values $y = f(x)$ derived
    from measured values $x$---which are distributed uniformly over the
    interval between 0~and~1---and for $f(x) = \frac{1+x}{1-x}$.%
  }\label{fig:q}
\end{figure}


\section{Example}

Suppose that
\begin{equation}
  f(x) = \frac{1+x}{1-x}
\end{equation}
\noindent and that
\begin{equation}
  p(x) = \left\{
    \begin{array}{ll}
      1 & 0 < x < 1\\
      0 & \mbox{otherwise.}
    \end{array}
    \right.
\end{equation}
\noindent Then measurements of $x$ will produce derived quantities $y = f(x)$
that range from 1~to infinity as $x$ changes from 0~to~1.  See
Figure~\ref{fig:f}.

To find the distribution of the derived quantities, begin by taking the
derivative of $f$.
\begin{equation}
  f'(x) = \frac{2}{{[1 - x]}^2}
\end{equation}
\noindent Next find the solutions to $f(x) = y$.  Because $f$ is one-to-one
over the domain of interest, there is a single, unique solution $x_1$ for each
$y.$
\begin{equation}
  x_1 = \frac{y - 1}{y + 1}
\end{equation}
\noindent Because $y$ varies between 1~and positive infinity, $x_1$ varies
between 0~and~1.  So $p(x_1) = 1$ for every $x_1$.  The final result is thus
\begin{equation}
  q(y) = \frac{1}{2}{\left[ 1 - \frac{y - 1}{y + 1} \right]}^2.
\end{equation}
\noindent See Figure~\ref{fig:q}.

\end{document}
