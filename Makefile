
.PHONY: all clean

all: prop-dist.pdf html

# --shell-escape is needed for gnuplottex

prop-dist.pdf: prop-dist.tex
	@pdflatex --shell-escape prop-dist
	@pdflatex --shell-escape prop-dist

# Both
# (A) second and third arguments to 'htlatex' and
# (B) contents of 'nma.cfg'
# were obtained from 'https://www.12000.org/my_notes/faq/LATEX/htse54.htm'.
html: prop-dist.pdf
	@mkdir -p html
	@rm -fv html/*
	@cp -v prop-dist.tex nma.cfg html
	@(cd html;\
	  htlatex\
	    prop-dist\
	    "nma.cfg,html,charset=utf-8"\
	    " -cunihtf -utf8"\
	    ""\
	    --shell-escape)

clean:
	@rm -frv html
	@rm -fv *gnuplot*
	@rm -fv *.out
	@rm -fv prop-dist.aux
	@rm -fv prop-dist.log
	@rm -fv prop-dist.pdf

