# The Problem

Suppose that, for a measured quantity $`x`$, one knows the probability-density
for the error in the measurement.

One wants an analytical expression of the probability-density for the error of
a derived quantity, such as $`x^2`$.

One could use the standard formula for propagation of error, but suppose that
one wants *not* to approximate the uncertainty as Gaussian.  (Even if the error
about $`x`$ be Gaussian, the error about $`x^2`$ would not be.)

If one resorted to Monte Carlo or to some other numerical method, then one
would fail to obtain the analytical result that one seeks.

What does one do?


# The Solution

The answer is here!

Suppose that $`f`$ is a differentiable function and that $`p`$ is the
probability-density of its input.

Here is a handy, analytical method for determining the probability-density
$`q`$, of $`f`$'s output:
```math
q(y) = \sum_k \frac{p(x_k)}{|f'(x_k)|},
```
where $`k`$ is the index over the solutions $`x_k`$ to the equation $`f(x) =
y`$.

I find the result by using Dirac's delta for a continuous distribution, in
analogy to the way in which Kronecker's delta can be used for a discrete
distribution.

I have written a document, which is available in HTML (experimental) or PDF.
The document shows some steps in deriving the result and provides a worked-out
example, to show how to make use of the expression (above) for $`q(y)`$.

- [HTML (experimental, might need insecure setting for math)](https://tevaughan.gitlab.io/propagation-of-distribution/index.html)
- [PDF](https://tevaughan.gitlab.io/propagation-of-distribution/prop-dist.pdf)


## Notes on Rendering of Mathematical Symbols in HTML-Document

The HTML-version is experimental because working with `htlatex` always seems
to involve more experimentation than I should like. Sometimes, no amount of
experimenting produces a result with which I am satisfied.

- At present, math is configured to render via mathjax, but this requires that
  the browser have access to the mathjax site.

- Math seems to work with Google's Chrome, Mozilla's Firefox, Microsoft's IE 11,
  and Microsoft's Edge.

- However, at least in Chrome and in Microsoft's browsers, the mathjax scripts
  are considered insecure; so the user might have to click an icon or button
  that enables insecure scripts in order to have the math be rendered
  properly.
